﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;

namespace Programming_Drawing
{
    public abstract class Shape
    {
        //variable declaration and initialization
        protected int x = 0, y = 0, z = 0;



        public Shape(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public Shape(int x, int y, int z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }


        public Shape()
        {
        }



        public void setX(int x)
        {
            this.x = x;
        }



        public int getX()
        {
            return x;
        }



        public void setY(int y)
        {
            this.y = y;
        }




        public int getY()
        {
            return y;
        }



        public abstract void draw(Graphics g, Color c, int thickness);
    }
}
